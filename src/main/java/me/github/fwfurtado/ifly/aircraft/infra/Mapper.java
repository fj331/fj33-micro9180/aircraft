package me.github.fwfurtado.ifly.aircraft.infra;

public interface Mapper<S, T> {
    T convert(S source);
}
