package me.github.fwfurtado.ifly.aircraft.features.creation;

import me.github.fwfurtado.ifly.aircraft.domain.Aircraft;

public interface CreationRepository {
    void save(Aircraft aircraft);
}
