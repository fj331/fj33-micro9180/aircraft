package me.github.fwfurtado.ifly.aircraft.features.creation;

import me.github.fwfurtado.ifly.aircraft.domain.Aircraft;
import me.github.fwfurtado.ifly.aircraft.infra.Mapper;
import org.springframework.stereotype.Component;

import static me.github.fwfurtado.ifly.aircraft.features.creation.CreationController.*;
import static me.github.fwfurtado.ifly.aircraft.infra.StringNormalizer.normalize;

@Component
class AircraftFormConverter implements Mapper<AircraftForm, Aircraft> {

    @Override
    public Aircraft convert(AircraftForm source) {
        var identifier = normalize(source.getCode());
        return new Aircraft(identifier, source.getModel(), source.getDescription());
    }
}
