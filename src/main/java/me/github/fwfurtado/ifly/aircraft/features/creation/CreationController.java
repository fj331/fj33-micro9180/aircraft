package me.github.fwfurtado.ifly.aircraft.features.creation;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("aircraft")
@AllArgsConstructor
class CreationController {

    private final CreationService service;

    @PostMapping
    ResponseEntity<?> createBy(@RequestBody AircraftForm form, UriComponentsBuilder uriBuilder) {
        var id = service.createAircraftBy(form);

        var uri = uriBuilder.path("aircraft/{code}").build(id);

        return created(uri).build();

    }

    @Data
    static class AircraftForm {
        private String code;
        private String description;
        private String model;
    }

}
