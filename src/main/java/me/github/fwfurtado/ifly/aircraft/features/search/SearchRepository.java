package me.github.fwfurtado.ifly.aircraft.features.search;

import me.github.fwfurtado.ifly.aircraft.domain.Aircraft;

import java.util.Optional;

public interface SearchRepository {

    Optional<Aircraft> findByIdentifier(String identifier);
}
