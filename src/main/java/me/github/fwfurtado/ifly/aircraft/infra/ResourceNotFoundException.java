package me.github.fwfurtado.ifly.aircraft.infra;

public class ResourceNotFoundException extends IllegalArgumentException {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
