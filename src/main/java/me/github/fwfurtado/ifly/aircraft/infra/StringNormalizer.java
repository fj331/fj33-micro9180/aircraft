package me.github.fwfurtado.ifly.aircraft.infra;

import java.text.Normalizer;

import static java.text.Normalizer.Form.NFD;

public class StringNormalizer {
    public static String normalize(String source) {
        return Normalizer.normalize(source.toUpperCase(), NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
