package me.github.fwfurtado.ifly.aircraft.features.search;

import lombok.AllArgsConstructor;
import me.github.fwfurtado.ifly.aircraft.domain.Aircraft;
import me.github.fwfurtado.ifly.aircraft.infra.ResourceNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;

import static me.github.fwfurtado.ifly.aircraft.infra.StringNormalizer.normalize;

@RestController
@RequestMapping("aircraft")
@AllArgsConstructor
class SearchController {

    private final SearchRepository repository;

    @GetMapping("{identifier}")
    Aircraft showBy(@PathVariable String identifier) {
        var message = MessageFormat.format("Cannot find an aircraft with the identifier %s", identifier);
        return repository.findByIdentifier(normalize(identifier)).orElseThrow(() -> new ResourceNotFoundException(message));
    }
}
