package me.github.fwfurtado.ifly.aircraft.features.creation;

import lombok.AllArgsConstructor;
import me.github.fwfurtado.ifly.aircraft.features.creation.CreationController.AircraftForm;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class CreationService {

    private final AircraftFormConverter converter;
    private final CreationRepository repository;


    public String createAircraftBy(AircraftForm form) {
       var aircraft = converter.convert(form);

       repository.save(aircraft);

       return aircraft.getIdentifier();
    }
}
