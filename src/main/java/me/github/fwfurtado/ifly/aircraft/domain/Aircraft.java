package me.github.fwfurtado.ifly.aircraft.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class Aircraft {
    @Id
    private String identifier;

    @NotNull
    private String mode;

    @NotNull
    private String description;
}
